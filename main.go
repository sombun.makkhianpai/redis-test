package main

import (
    "context"
	"fmt"
	"time"
	// "errors"


    "github.com/go-redis/redis/v8"
)

var ctx = context.Background()


func ExampleClient() {
    rdb := redis.NewClient(&redis.Options{
        Addr:     "localhost:6379",
        Password: "", // no password set
        DB:       0,  // use default DB
    })

    // err := rdb.Set(ctx, "key", "value", 0).Err()
    // if err != nil {
    //     panic(err)
    // }

    // val, err := rdb.Get(ctx, "key").Result()
    // if err != nil {
    //     panic(err)
    // }
    // fmt.Println("key", val)

    // val2, err := rdb.Get(ctx, "key2").Result()
    // if err == redis.Nil {
    //     fmt.Println("key2 does not exist")
    // } else if err != nil {
    //     panic(err)
    // } else {
    //     fmt.Println("key2", val2)
    // }
    // Output: key value
    // key2 does not exist

	// SET key value EX 10 NX
	set, err := rdb.SetNX(ctx, "key4", "value", 10*time.Second).Result()
	// if err != nil {
    //     panic(err)
    // }

	// // SET key value keepttl NX
	set, err = rdb.SetNX(ctx, "key4", "value", redis.KeepTTL).Result()
	if err != nil {
        panic(err)
    }
	fmt.Println(set)
	// // SORT list LIMIT 0 2 ASC
	// vals, err := rdb.Sort(ctx, "list", &redis.Sort{Offset: 0, Count: 2, Order: "ASC"}).Result()

	// // ZRANGEBYSCORE zset -inf +inf WITHSCORES LIMIT 0 2
	// vals, err := rdb.ZRangeByScoreWithScores(ctx, "zset", &redis.ZRangeBy{
	// 	Min: "-inf",
	// 	Max: "+inf",
	// 	Offset: 0,
	// 	Count: 2,
	// }).Result()

	// // ZINTERSTORE out 2 zset1 zset2 WEIGHTS 2 3 AGGREGATE SUM
	// vals, err := rdb.ZInterStore(ctx, "out", &redis.ZStore{
	// 	Keys: []string{"zset1", "zset2"},
	// 	Weights: []int64{2, 3}
	// }).Result()

	// // EVAL "return {KEYS[1],ARGV[1]}" 1 "key" "hello"
	// vals, err := rdb.Eval(ctx, "return {KEYS[1],ARGV[1]}", []string{"key"}, "hello").Result()

	// // custom command
	// res, err := rdb.Do(ctx, "set", "key", "value").Result()
}

func exTestRedis() {
	// Way 1 to Connecting to Redis Server
	// rdb := redis.NewClient(&redis.Options{
    //     Addr:     "localhost:6379",
    //     Password: "", // no password set
    //     DB:       0,  // use default DB
    // })

	// Way 2 to Connecting to Redis Server
	opt, err := redis.ParseURL("redis://localhost:6379/0")
	if err != nil {
		panic(err)
	}

	rdb := redis.NewClient(opt)

	//redis.Nil is return for case key doesn't exist, this case for command that can return redis.Nil example GET, BLPOP and ZSCORE
	// val, err := rdb.Get(ctx, "key5").Result()
	// switch {
	// case err == redis.Nil:
	// 	fmt.Println("key does not exist")
	// 	fmt.Println(err)
	// case err != nil:
	// 	fmt.Println("Get failed", err)
	// case val == "":
	// 	fmt.Println("value is empty")
	// }

	// fmt.Println(val)

	// Shortcut for get.Val().(string) with error handling.
	// get := rdb.Get(ctx, "key5")
	// fmt.Println(get)
	// s, err := get.Text()
	// fmt.Println(s)
	// fmt.Println(err)

	// num, err := get.Int()
	// fmt.Println(num)
	// fmt.Println(err)

	// num, err := get.Int64()

	// num, err := get.Uint64()

	// num, err := get.Float32()
	// fmt.Println(num)
	// fmt.Println(err)

	// num, err := get.Float64()

	// flag, err := get.Bool()

	// To execute arbitrary/custom command:
	// val, err := rdb.Do(ctx, "set", "key6", "item").Result()
	// if err != nil {
	// 	if err == redis.Nil {
	// 		fmt.Println("key does not exists")
	// 		return
	// 	}
	// 	panic(err)
	// }

	// val, err = rdb.Do(ctx, "get", "key6").Result()
	// if err != nil {
	// 	if err == redis.Nil {
	// 		fmt.Println("key does not exists")
	// 		return
	// 	}
	// 	panic(err)
	// }
	// fmt.Println(val.(string))

	// To execute several commands in a single write/read pipeline:
	// pipe := rdb.Pipeline()

	// incr := pipe.Incr(ctx, "pipeline_counter")
	// pipe.Expire(ctx, "pipeline_counter", time.Hour)
	// result := pipe.Get(ctx, "key5")

	// _, err = pipe.Exec(ctx)
	// if err != nil {
	// 	panic(err)
	// }

	// // The value is available only after Exec.
	// fmt.Println(incr.Val())
	// fmt.Println(result.Val())


// Increment transactionally increments key using GET and SET commands.
	// const maxRetries = 1000
// 	increment := func(key string) error {
// 		// Transactional function.
// 		txf := func(tx *redis.Tx) error {
// 			// Get current value or zero.
// 			n, err := tx.Get(ctx, key).Int()
// 			if err != nil && err != redis.Nil {
// 				return err
// 			}

// 			// Actual operation (local in optimistic lock).
// 			n++

// 			// Operation is commited only if the watched keys remain unchanged.
// 			_, err = tx.TxPipelined(ctx, func(pipe redis.Pipeliner) error {
// 				pipe.Set(ctx, key, n, 0)
// 				return nil
// 			})
// 			return err
// 		}

// 		for i := 0; i < maxRetries; i++ {
// 			err := rdb.Watch(ctx, txf, key)
// 			if err == nil {
// 				// Success.
// 				return nil
// 			}
// 			if err == redis.TxFailedErr {
// 				// Optimistic lock lost. Retry.
// 				continue
// 			}
// 			// Return any other error.
// 			return err
// 		}

// 		return errors.New("increment reached maximum number of retries")
// 	}

// err = increment("key5")

// fmt.Println(err)

// PubSub
// go-redis allows to publish messages and subscribe to channels. It also automatically handles reconnects.

	// err = rdb.Publish(ctx, "mychannel1", "payload").Err()
	// if err != nil {
	// 	panic(err)
	// }

	// pubsub := rdb.Subscribe(ctx, "mychannel1")
	// for {
	// 	msg, err := pubsub.ReceiveMessage(ctx)
	// 	if err != nil {
	// 		panic(err)
	// 	}
	
	// 	fmt.Println(msg.Channel, msg.Payload)
	// }

	// Delete key
	key := "key5"
	key2 := "key6"
	var result int64
	result, err = rdb.Del(ctx, key, key2).Result()

	fmt.Println(result)

}

func main() {
	exTestRedis()
	// rdb := redis.NewClient(&redis.Options{
    //     Addr:     "localhost:6379",
    //     Password: "", // no password set
    //     DB:       0,  // use default DB
    // })

    // err := rdb.Set(ctx, "key", "value2", 0).Err()
    // if err != nil {
    //     panic(err)
    // }
}